<?php

	//get the image
	$text = $_GET['t'];
	
	$directory = './images';
	$file_name = $directory.'/im_'.$text.'.png';
	
	if(!@file_exists($directory)) {
		
		mkdir($directory);
		
	} else {
		
		if(@file_exists($file_name)) {
			header('Location: '.$file_name);
			exit;
		}
		
	}
	
	//create the image
	$font_size = 12;
	$font_angle = 0;
	$image_width = 70;
	$image_height = 30;
	
	putenv('GDFONTPATH=' . realpath('.'));
	$font = './myriad.ttf';
	
	$bbox = imagettfbbox($font_size, $font_angle, $font, $text);

	$text_width = $bbox[2] - $bbox[0];
	$text_height = $bbox[3] - $bbox[5];
	if($text_width > $image_width) {
		$image_width = $text_width;
	}
	
	$im = imagecreatetruecolor($image_width,$image_height);
	imagesavealpha($im,true);
	$black = imagecolorallocatealpha($im, 0, 0, 0, 0);
	$transparent = imagecolorallocatealpha($im, 0,0,0,127);
	imagefill($im,0,0,$transparent);
	
	// Write it
	imagettftext($im, $font_size, $font_angle, ($image_width-$text_width)/2, (($image_height-$text_height)/2)+$text_height, $black, $font, $text);
	
	$output = imagepng($im,$file_name);
	imagedestroy($im);
	
	if($output) {
		header('Content-Type: image/png');
		readfile($file_name);
	}