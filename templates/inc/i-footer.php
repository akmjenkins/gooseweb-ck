			<footer class="dark-bg">
				<div class="sw">
				
					<div class="footer-blocks">
						<div class="footer-block">
						
							<a href="#" class="footer-logo">
								<img src="../assets/images/goose-pond-country-properties.svg" alt="Goose Pond Country Properties Logo">
							</a>
						
							<div class="footer-contact">
								
								<address>
									123 Your Street <br />
									Whitebourne, NL A1B 2C3
								</address>
								
								<div class="rows">
									<div class="row">
										<span class="l">P</span>
										<span class="r">1 (555) 555-5555</span>
									</div>
									<div class="row">
										<span class="l">TF</span>
										<span class="r">1 (800) 555-5555</span>
									</div>
								</div>
								
							</div><!-- .footer-contact -->
						
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="footer-nav">
								<ul>
									<li><a href="#">Goose Pond Properties</a></li>
									<li><a href="#">Services</a></li>
									<li><a href="#">Other Projects</a></li>
									<li><a href="#">The Latest</a></li>
								</ul>
							</div><!-- .footer-nav -->
							
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="footer-callout">
								<p>Find your dream property today at Goose Pond.</p>
								<a href="#" class="button big">Now Selling</a>
							</div><!-- .center -->
						
						
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Goose Pond Properties</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->

			<?php include('i-map-overlay.php'); ?>
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/gooseweb-ck/templates',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>