		<div class="t-fa-abs fa-refresh gooseweb-map-overlay">
		
			<div class="gooseweb-map-header dark-bg">
			
				<div class="selector with-arrow">
					<select id="phase-selector">
						<option value="">Select A Phase</option>
						<option value="">Show All Phases</option>
					</select>
					<span class="value">&nbsp;</span>
				</div><!-- .selector -->
				
				<button class="t-fa-abs fa-close toggle-map-overlay">Close</button>
				
			</div><!-- .gooseweb-map-header -->
		
			<div class="gooseweb-map" data-center="44.9868465,-64.1363655" data-zoom="12">
			</div><!-- .gooseweb-map -->
			
			<div class="mobile-infowindow custom-info-window-content"></div>
		</div><!-- .gooseweb-map-overlay -->