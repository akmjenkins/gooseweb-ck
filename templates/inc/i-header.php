<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Goose Pond Country Properties</title>
		<meta charset="utf-8">

		<?php 
			//for embedded google map
			include('inc/api-keys.php');		
		?>
		
		<!-- jQuery -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/images/favicons/favicon-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">

		<!-- slickslider -->
		<link rel="stylesheet" href="../bower_components/slick.js/slick/slick.css">
		<script src="../bower_components/slick.js/slick/slick.min.js"></script>
		
		<link rel="stylesheet" href="../assets/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					<a href="#" class="header-logo">
						<img src="../assets/images/goose-pond-country-properties-logo-notext.svg" alt="Goose Pond Country Properties Logo" class="emblem">
						<img src="../assets/images/goose-pond-country-properties-text-logo.svg" alt="Goose Pond Country Properties Logo" class="text">
					</a>
				</div><!-- .sw -->
			</header>