;(function(context) {
	
	var HTMLMarker;

	var HTMLMarkerClosure = function(opts) {
		HTMLMarker = function(opts) {
			this.opts = $.extend({
				selectedClass: 'selected',
				"class": 'map-marker',
				extraClasses: ''
			},opts);
			
			this.markerDiv = this.getMarkerDiv();
			this.listener = null;
			
			if(opts.map) {
				this.setMap(opts.map);
			}
		};
		
		HTMLMarker.prototype = new google.maps.OverlayView();
		
		HTMLMarker.prototype.getMarkerDiv = function() {
			return $('<div class="'+this.opts['class']+' ' + this.opts['extraClasses'] + '" title="'+(this.opts.title || '')+'">'+(this.opts.content || '')+'</div>')[0];
		}
		
		HTMLMarker.prototype.onAdd = function() {
			var self = this;
			if(!this.markerDiv) {
				this.markerDiv = this.getMarkerDiv();
			}
			this.getPanes().floatPane.appendChild(this.markerDiv);
			this.listener = google.maps.event.addDomListener(this.markerDiv, 'click', function() { google.maps.event.trigger(self, 'click'); });
		};
		
		HTMLMarker.prototype.onRemove = function() {
			this.markerDiv.parentNode.removeChild(this.markerDiv);
			this.markerDiv = null;
			google.maps.event.removeListener(this.listener);
		};
		
		HTMLMarker.prototype.draw = function() {
			var 
				projection = this.getProjection(),
				center = projection.fromLatLngToDivPixel(this.opts.position),
				w = parseInt($(this.markerDiv).css('width'),10),
				h = parseInt($(this.markerDiv).css('height'),10);
				
				this.markerDiv.style.top = center.y-(h/2)+'px';
				this.markerDiv.style.left = center.x-(w/2)+'px';
			
		};
		
		HTMLMarker.prototype.setSelected = function(selected) {
			if(selected) {
				$(this.markerDiv).addClass(this.opts.selectedClass);
			} else {
				$(this.markerDiv).removeClass(this.opts.selectedClass);
			}
			
			//the div has changed size
			this.draw();
			
		};
		
		HTMLMarker.prototype.isSelected = function() {
			return $(this.markerDiv).hasClass(this.opts.selectedClass);
		};
		
		HTMLMarker.prototype.getPosition = function() {
			return this.opts.position
		};		

		return new HTMLMarker(opts);
	};

	(function() {
		var cb = function(opts) {
			return HTMLMarker ? new HTMLMarker(opts) : HTMLMarkerClosure(opts);
		}
		
		//CommonJS
		if(typeof module !== 'undefined' && module.exports) {
			module.exports = cb;
		//CodeKit
		} else if(context) {
			context.HTMLMarker = cb;
		}
		
	}());
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));